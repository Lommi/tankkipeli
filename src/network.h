#ifndef NETWORK_H
#define NETWORK_H
#include <iostream>
#include <cstdlib>
#include "globals.h"
#include "game.h"
//#include <pthread.h> For UNIX

using namespace std;

#define BUFLEN 512
#define PORT 10000

class Network;
class Game;

struct sockinfo
{
     sockaddr_in address;
     sockaddr &address_cast   = (sockaddr&)address;
     socklen_t addr_len;
     char ip_addr[32];
};

class Network
{
    public:
    Network();
    ~Network();

     int sockfd;

    int result;
    int numbytes;
    int enable = 1;
    int tmp;

    sockinfo thispc;
    sockinfo theirpc;

    char sendbuf[BUFLEN] = "hehe";
    char recvbuf[BUFLEN];
    char *p; //used for parsing received data

    bool network_active;
    bool init_sockets();
    bool sendmsg();
    void update(Game *game);

    void *get_in_addr(struct sockaddr *sa);

    void shutdown_socket();
};

#endif
