#include "input.h"

void Input::update(Game *game, Renderer *renderer, Assets *assets, Network *network)
{
    keystate = SDL_GetKeyboardState(NULL);

    //continuous-response keys
    if(keystate[SDL_SCANCODE_W])
    {
        game->player.bodyangle -= game->player.turnspeed;
        game->player.turretangle -= game->player.turnspeed;
        game->player.x += cos(game->player.bodyangle * RADIAN) * game->player.drivespeed;
        game->player.y += sin(game->player.bodyangle * RADIAN) * game->player.drivespeed;
    }
    else if(keystate[SDL_SCANCODE_D])
    {
        game->player.bodyangle += game->player.turnspeed;
        game->player.turretangle += game->player.turnspeed;
        game->player.x -= cos(game->player.bodyangle * RADIAN) * game->player.drivespeed;
        game->player.y -= sin(game->player.bodyangle * RADIAN) * game->player.drivespeed;
    }
    if(keystate[SDL_SCANCODE_O])
    {
        game->player.bodyangle += game->player.turnspeed;
        game->player.turretangle += game->player.turnspeed;
        game->player.x += cos(game->player.bodyangle * RADIAN) * game->player.drivespeed;
        game->player.y += sin(game->player.bodyangle * RADIAN) * game->player.drivespeed;
    }
    else if(keystate[SDL_SCANCODE_K])
    {
        game->player.bodyangle -= game->player.turnspeed;
        game->player.turretangle -= game->player.turnspeed;
        game->player.x -= cos(game->player.bodyangle * RADIAN) * game->player.drivespeed;
        game->player.y -= sin(game->player.bodyangle * RADIAN) * game->player.drivespeed;
    }
    if(keystate[SDL_SCANCODE_Q])
    {
        game->player.bodyangle -= game->player.turnspeed;
        game->player.turretangle -= game->player.turnspeed;
    }
    if(keystate[SDL_SCANCODE_P])
    {
        game->player.bodyangle += game->player.turnspeed;
        game->player.turretangle += game->player.turnspeed;
    }
    if(keystate[SDL_SCANCODE_E])
        game->player.turretangle -= game->player.turnspeed;
    if(keystate[SDL_SCANCODE_I])
        game->player.turretangle += game->player.turnspeed;

    SDL_Event e;
    int x, y;
    while(SDL_PollEvent(&e) != 0)
    {
        switch (e.type)
        {
            case (SDL_QUIT):
            {
                game->mainloop = 0;
            }break;
            case (SDL_KEYDOWN):
            {
                switch (e.key.keysym.sym)
                {
                    case SDLK_ESCAPE: game->mainloop = 0; break;
                    case SDLK_1: renderer->set_fullscreen(0); break;
                    case SDLK_2: renderer->set_fullscreen(1); break;
                    case SDLK_5: game->enemy.dead = 0; break;
                    case SDLK_6: game->player.dead = 0; break;
                    case SDLK_m: game->player.turret_origin.x += 1;
                        printf("tox: %d\n", game->player.turret_origin.x); break;
                    case SDLK_n: game->player.turret_origin.x -= 1;
                        printf("tox: %d\n", game->player.turret_origin.x); break;
                    case SDLK_b: game->player.turret_origin.y += 1;
                        printf("toy: %d\n", game->player.turret_origin.y); break;
                    case SDLK_v: game->player.turret_origin.y -= 1;
                        printf("toy: %d\n", game->player.turret_origin.y); break;
                    case SDLK_SPACE:
                    {
                        if (!space_pressed)
                        {
                            space_pressed = 1;
                            for (int i = 0; i < game->bullets.size() / 2; ++i)
                            {
                                if (!game->bullets[i]->active)
                                {
                                    game->bullets[i]->id = i;
                                    game->bullets[i]->x = game->player.x
                                        + game->player.turret_origin.x
                                        - cos(game->player.turretangle * RADIAN) * 36;
                                    game->bullets[i]->y = game->player.y
                                        + game->player.turret_origin.y - 5
                                        - sin(game->player.turretangle * RADIAN) * 36;
                                    game->bullets[i]->angle = game->player.turretangle;
                                    game->bullets[i]->active = 1;
                                    sprintf(network->sendbuf, "%d %d %d %d %d", 2, i, (int)game->bullets[i]->x, (int)game->bullets[i]->y, (int)game->player.turretangle);
                                    network->sendmsg();
                                    break;
                                }
                            }
                        }
                    }break;
                    case SDLK_RETURN:
                    {
                        if (game->gamewinner != 0)
                        {
                            sprintf(network->sendbuf, "3");
                            network->sendmsg();
                            game->init_stats();
                        }
                    }break;
                }
            }break;
            case (SDL_KEYUP):
            {
                switch (e.key.keysym.sym)
                {
                    case SDLK_SPACE: space_pressed = 0; break;
                }
            }break;
            case (SDL_MOUSEBUTTONDOWN):
            {
                switch (e.button.button)
                {
                    case SDL_BUTTON_LEFT:
                    {
                    }break;
                }
            }break;
        }
    }
    SDL_PumpEvents();
}

void Input::menu(Game *game)
{
    SDL_Event e;
    while(SDL_PollEvent(&e) != 0)
    {
        switch (e.type)
        {
            case (SDL_QUIT):
            {
                game->mainloop = 0;
            }break;
            case (SDL_KEYDOWN):
            {
                switch (e.key.keysym.sym)
                {
                    case SDLK_ESCAPE: game->state = 1; game->mainloop = 0; break;
                    case SDLK_RETURN: game->state = 1; break;
                }
            }
        }
    }
}
