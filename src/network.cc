#include "globals.h"
#include "network.h"

Network::Network()
{
    #if PLATFORM == PLATFORM_WINDOWS
    WSADATA WsaData;
    result = WSAStartup(MAKEWORD(2, 2), &WsaData);
    if (result != NO_ERROR)
        printf("WSAStartup failed with error: %d\n", result);
    #endif
}

Network::~Network()
{
    #if PLATFORM == PLATFORM_WINDOWS
    WSACleanup();
    #endif
}

bool Network::init_sockets()
{
    strcpy(thispc.ip_addr,  "127.0.0.1");
    strcpy(theirpc.ip_addr, "127.0.0.1");

    FILE *f;
    f = fopen("config.conf", "r");
    fscanf(f, "%s", thispc.ip_addr);
    fscanf(f, "%s", theirpc.ip_addr);
    fclose(f);
    printf("thispc.ip_addr: %s\ntheirpc.ip_addr: %s\n",
        thispc.ip_addr, theirpc.ip_addr);

    sockfd = socket(AF_INET, SOCK_DGRAM, 0); if (sockfd < 0) printf("socket creation failed\n");

    thispc.address.sin_family      = AF_INET;
    thispc.address.sin_addr.s_addr = inet_addr(thispc.ip_addr);
    thispc.address.sin_port        = htons((unsigned short)PORT);
    thispc.addr_len = sizeof(thispc.address);
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (char*)&enable, sizeof(int));
    if (bind(sockfd, &thispc.address_cast, sizeof(thispc.address)) < 0) printf("bind failed\n");

    theirpc.address.sin_family      = AF_INET;
    theirpc.address.sin_addr.s_addr = inet_addr(theirpc.ip_addr);
    theirpc.address.sin_port        = htons((unsigned short)PORT);
    connect(sockfd, &theirpc.address_cast, sizeof(theirpc.address));

    #if PLATFORM == PLATFORM_MAX || \
        PLATFORM == PALTFORM_UNIX

        int non_blocking = 1;
        if (fcntl(sockfd, F_SETFL, O_NONBLOCK, non_blocking) == -1)
            printf("failed to set non-blocking\n");

    #elif PLATFORM == PLATFORM_WINDOWS
        DWORD non_blocking = 1;
        if (ioctlsocket(sockfd, FIONBIO, &non_blocking) != 0)
            printf("failed to set non-blocking\n");
    #endif

    return 0;
}
bool Network::sendmsg()
{
    send(sockfd, sendbuf, sizeof(sendbuf), 0);
    //printf("sent: %s\n", sendbuf);
    return 0;
}
void Network::update(Game *game)
{
    while(network_active)
    {
        memset(recvbuf, '\0', BUFLEN);
        theirpc.addr_len = sizeof(theirpc.address);
        numbytes = recv(sockfd, recvbuf, BUFLEN, 0);
        if (numbytes > 0)
        {
            //printf("received: %s\n", recvbuf);
            p = recvbuf;
            tmp = atoi(p);
            switch(tmp)
            {
                case 1: //movement
                {
                    p += sizeof(char);
                    game->enemy.x = atof(p);
                    p += sizeof(int);
                    game->enemy.y = atof(p);
                    p += sizeof(int);
                    game->enemy.bodyangle = atof(p);
                    p += sizeof(int);
                    game->enemy.turretangle = atof(p);
                }break;
                case 2: //create bullet
                {
                    p += sizeof(char) * 2;
                    int bulletid = atoi(p);
                    game->bullets[10 + bulletid]->active = 1;
                    game->bullets[10 + bulletid]->id = 10 + bulletid;
                    p += sizeof(char) * 2;
                    game->bullets[10 + bulletid]->x = atoi(p);
                    p += sizeof(int);
                    game->bullets[10 + bulletid]->y = atoi(p);
                    p += sizeof(int);
                    game->bullets[10 + bulletid]->angle = atoi(p);
                }break;
                case 3: //restart game
                {
                    game->init_stats();
                }break;
            }
        }
    }
}

void Network::shutdown_socket()
{
    #if PLATFORM == PLATFORM_MAX || \
        PLATFORM == PLATFORM_UNIX
        close(sockfd);
        printf("Socket closed\n");
    #elif PLATFORM == PLATFORM_WINDOWS
        closesocket(sockfd);
        printf("Socket closed\n");
    #endif

    network_active = 0;
    printf("network shutdown\n");
}

void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET)
        return &(((struct sockaddr_in*)sa)->sin_addr);
    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}
