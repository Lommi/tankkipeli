#pragma once
#include "types.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <float.h>

extern float    F_COLOR_WHITE[4];
extern float    F_COLOR_BLACK[4];
extern float    F_COLOR_SYS_BLUE[4];

extern Color    BYTE_COLOR_WHITE;
//extern vec3     DEFAULT_UP_VECTOR;

#define PI 3.1415926535897
#define RADIAN PI / 180

#define MIN(val, min)\
    (val < min ? min : val)

#define MAX(val, max)\
    (val > max ? max : val)

#define ABS(val)\
    (val < 0 ? -1 * val : val)

#define CEIL(val)\
    (val - (float)(int)val > 0.0f ? (float)((int)(val) + 1) : val)

#define CLAMP(val, min, max)\
    (MIN(MAX(val, max), min))

#define POWER_OF_2(val) ((val) * (val))

#define DEG_TO_RAD(degrees)\
    ((double)degrees * (double)(M_PI / 180.0f))

#define RAD_TO_DEG(rad)\
	((double)rad * (double)(180.0f / M_PI))

static inline float
RADIAN_CLAMPF(float val);

#define TEST_POINT_IN_RECT(point_x, point_y, x, y, w, h) \
    (  point_x >= x && point_x <= x + w \
    && point_y >= y && point_y <= y + h)

#define SET_BITFLAG_ON(val, val_type, flag) (val = val | (val_type)flag)
#define SET_BITFLAG_OFF(val, val_type, flag) (val = val & ~(val_type)flag);
#define GET_BITFLAG(val, val_type, flag) ((val & (val_type)flag) == (val_type)flag)

#define KILOBYTES(num_bytes) (num_bytes * 1000)
#define MEGABYTES(num_bytes) (num_bytes * 1000000)

#define SIZE_OF_VERTEX (8 * sizeof(GLfloat))
