#pragma once
#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#include <assert.h>
#include <stdint.h>

#if _MSC_VER == 1800
	#define inline __inline
#endif

#ifdef _DEBUG

    #include <stdio.h>
    #define DEBUG_PRINTF(str, ...)\
        fprintf(stdout, str, ##__VA_ARGS__)

    #define SET_BREAKPOINT() DebugBreak()

#else
    #define DEBUG_PRINTF(str, ...)
    #define SET_BREAKPOINT()
#endif

#define ASSERT(expression) assert(expression)
#define SYS_MEMORY_BARRIER() MemoryBarrier()

#define thread_ret_t DWORD WINAPI

typedef struct Thread Thread;
typedef CRITICAL_SECTION Mutex;
typedef struct ConditionVariable ConditionVariable;
typedef SOCKET sys_socket_t;

static inline void
Thread_create(Thread *thread, LPTHREAD_START_ROUTINE func, void *args);

static inline void
Thread_join(Thread *thread);

static inline void
Thread_detach(Thread *thread);

static inline void
Thread_kill(Thread *thread);

static inline void
Mutex_init(Mutex *mutex);

static inline void
Mutex_lock(Mutex *mutex);

static inline void
Mutex_unlock(Mutex *mutex);

static inline void
ConditionVariable_init(ConditionVariable *cond_var);

static inline void
ConditionVariable_wait(ConditionVariable *cond_var, Mutex *mutex);

static inline void
ConditionVariable_notifyOne(ConditionVariable *cvar);

static inline void
sys_sleepMilliseconds(unsigned int ms);

static inline void *
sys_allocateMemory(size_t size);

static inline int
sys_getSockErrorString(char *buf, int buf_len);

static inline int
sys_initSockAPI();

static inline int
sys_closeSocket(sys_socket_t sock);

static inline void
sys_setSocketNonBlocking(sys_socket_t sock);

struct Thread
{
    HANDLE  handle;
    DWORD   id;
};

struct ConditionVariable
{
    CRITICAL_SECTION    *critical_section;
    CONDITION_VARIABLE  native_handle;
};

static inline void
Thread_create(Thread *thread, LPTHREAD_START_ROUTINE func, void *args)
{
    thread->handle = CreateThread(0, 0, func, args, 0, &thread->id);
}

static inline void
Thread_join(Thread *thread)
{
    WaitForSingleObject(thread->handle, INFINITE);
}

static inline void
Thread_detach(Thread *thread)
{
    CloseHandle(thread->handle);
}

static inline void
Thread_kill(Thread *thread)
{
    TerminateThread(thread->handle, THREAD_TERMINATE);
}

static inline void
Mutex_init(Mutex *mutex)
{
    InitializeCriticalSection(mutex);
}

static inline void
Mutex_lock(Mutex *mutex)
{
    EnterCriticalSection(mutex);
}

static inline void
Mutex_unlock(Mutex *mutex)
{
    LeaveCriticalSection(mutex);
}

static inline void
ConditionVariable_wait(ConditionVariable *cvar, Mutex *mutex)
{
    cvar->critical_section = mutex;
    EnterCriticalSection(cvar->critical_section);
    SleepConditionVariableCS(&cvar->native_handle, cvar->critical_section, INFINITE);
}

static inline void
ConditionVariable_notifyOne(ConditionVariable *cvar)
{
    WakeConditionVariable(&cvar->native_handle);
    LeaveCriticalSection(cvar->critical_section);
}

static inline void
ConditionVariable_init(ConditionVariable *cond_var)
{
    InitializeConditionVariable(&cond_var->native_handle);
}

static inline void
sys_sleepMilliseconds(unsigned int ms)
{
    Sleep(ms);
}

static inline void *
sys_allocateMemory(size_t size)
{
    return VirtualAlloc(0, size, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
}

static inline int
sys_getSockErrorString(char *buf, int buf_len)
{
    wchar_t *str = 0;

    FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        0,
        WSAGetLastError(),
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPWSTR)&str, 0, 0);
    wcstombs(buf, str, buf_len);
    LocalFree(str);

    return strlen(buf);
}

static inline int
sys_initSockAPI()
{
    WSADATA wsa_data;
    return WSAStartup(MAKEWORD(2, 2), &wsa_data);
}

static inline int
sys_closeSocket(sys_socket_t sock)
{
    return closesocket(sock);
}

static inline void
sys_setSocketNonBlocking(sys_socket_t sock)
{
    unsigned long mode = 1;
    ioctlsocket(sock, FIONBIO, &mode);
}
