#pragma once
#include <stdint.h>
#include <SDL2/SDL_ttf.h>

typedef unsigned int        uint;
typedef long unsigned int   long_uint;
typedef int                 bool32;
typedef char                bool8;
typedef unsigned char       ubyte;
typedef unsigned short      ushort;
typedef int8_t              int8;
typedef int16_t             int16;
typedef int32_t             int32;
typedef int64_t             int64;
typedef uint8_t             uint8;
typedef uint16_t            uint16;
typedef uint32_t            uint32;
typedef uint64_t            uint64;
typedef TTF_Font            Font;
typedef SDL_Color           Color;
