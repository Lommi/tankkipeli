#include "game.h"

bool Game::init_stats()
{
    mainloop = 1;
    num_bullets = 20;
    bullet_idx = 0;

    gamewinner = 0;

    for (int i = 0; i < bullets.size(); ++i)
        bullets[i]->active = 0;

    p1_hp = 200;
    p2_hp = 200;
    player.dead = 0;
    enemy.dead  = 0;

    hp1_rec.x = 64;
    hp1_rec.y = 16;
    hp1_rec.w = p1_hp;
    hp1_rec.h = 16;
    memcpy(&hpbg1_rec, &hp1_rec, sizeof(int) * 4);

    hp2_rec.x = display_size_w - 264;
    hp2_rec.y = 16;
    hp2_rec.w = p2_hp;
    hp2_rec.h = 16;
    memcpy(&hpbg2_rec, &hp2_rec, sizeof(int) * 4);

    return 0;
}

Player Game::create_player(Sprite *bodysprite, Sprite *turretsprite, int x, int y)
{
    Player player;
    player.bodysprite = bodysprite;
    player.turretsprite = turretsprite;
    player.x = x;
    player.y = y;
    player.turret_xoffset = 4;
    player.turret_yoffset = 4;
    player.bodyangle = 0.f;
    player.turretangle = 180.0f;
    player.body_origin.x = 16;
    player.body_origin.y = 24;
    player.turret_origin.x = 16;
    player.turret_origin.y = 24;
    return player;
}

int Game::create_bullet(Sprite *sprite, int x, int y, float speed, double angle)
{
    Bullet *bullet = new Bullet;
    bullet->sprite = sprite;
    bullet->x = x;
    bullet->y = y;
    bullet->speed = speed;
    bullet->angle = angle;
    bullet->active = 0;
    bullets.push_back(bullet);
    return 0;
}

void Game::update(Renderer *renderer, Network *network)
{
    player.x += cos(player.bodyangle * RADIAN) * 2;
    player.y += sin(player.bodyangle * RADIAN) * 2;
    for (int i = 0; i < bullets.size(); ++i)
    {
        if (!bullets[i]->active) continue;
        bullets[i]->x -= cos(bullets[i]->angle * RADIAN) * bullets[i]->speed;
        bullets[i]->y -= sin(bullets[i]->angle * RADIAN) * bullets[i]->speed;

        if (bullets[i]->x >= renderer->display_size.w ||
            bullets[i]->x <= 0 ||
            bullets[i]->y >= renderer->display_size.h ||
            bullets[i]->y <= 0) bullets[i]->active = 0;

        if (bullets[i]->x >= enemy.x + enemy.bodysprite->clip->x &&
            bullets[i]->x <= enemy.x + enemy.bodysprite->clip->w &&
            bullets[i]->y >= enemy.y + enemy.bodysprite->clip->y &&
            bullets[i]->y <= enemy.y + enemy.bodysprite->clip->h &&
            i < 10 && bullets[i]->active)
        {
            if (p2_hp > 0)
            {
                bullets[i]->active = 0;
                p2_hp -= 20;
                hp2_rec.w = p2_hp;
            }
            if (p2_hp <= 0)
            {
                enemy.dead = 1;
                gamewinner = 1;
            }
        }

        if (bullets[i]->x >= player.x + player.bodysprite->clip->x &&
            bullets[i]->x <= player.x + player.bodysprite->clip->w &&
            bullets[i]->y >= player.y + player.bodysprite->clip->y &&
            bullets[i]->y <= player.y + player.bodysprite->clip->h &&
            i >= 10 && bullets[i]->active)
        {
            if (p1_hp > 0)
            {
                bullets[i]->active = 0;
                p1_hp -= 20;
                hp1_rec.w = p1_hp;
            }
            if (p1_hp <= 0)
            {
                player.dead = 1;
                gamewinner = 2;
            }
        }
    }

    if (player.bodyangle >= 360)
        player.bodyangle = 0;
    if (player.bodyangle < 0)
        player.bodyangle = 360;
    if (player.turretangle >= 360)
        player.turretangle = 0;
    if (player.turretangle < 0)
        player.turretangle = 360;

    if (enemy.bodyangle >= 360)
        enemy.bodyangle = 0;
    if (enemy.bodyangle < 0)
        enemy.bodyangle = 360;
    if (enemy.turretangle >= 360)
        enemy.turretangle = 0;
    if (enemy.turretangle < 0)
        enemy.turretangle = 360;

    sprintf(network->sendbuf, "%d %d %d %d %d", 1, (int)player.x, (int)player.y,
        (int)player.bodyangle, (int)player.turretangle);
    network->sendmsg();
}
