#include "graphics.h"

Renderer::Renderer()
{
    window   = 0;
    renderer = 0;


    if (SDL_Init(SDL_INIT_EVERYTHING) < 0){ printf("SDL_Init failure, SDL_Error: %s\n", SDL_GetError()); exit(0); }

    //display_size = get_display_size(0);
    display_size.w = 1280;
    display_size.h = 720;
    printf("dsw: %d, dsh: %d\n", display_size.w, display_size.h);

    window = SDL_CreateWindow("Tankkipeli",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        display_size.w, display_size.h,
        SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);

    if (!window){ printf("SDL_CreateWindow failure, SDL Error: %s\n", SDL_GetError()); exit(0); }

    renderer = SDL_CreateRenderer(window, -1,
        SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    if (!renderer){ printf("SDL_CreateRenderer failure, SDL Error: %s\n", SDL_GetError()); exit(0); }

    int imgFlags = IMG_INIT_PNG;
    if (!(IMG_Init(imgFlags) & imgFlags)) { printf("SDL_Image init failure, SDL_Image Error: %s\n", IMG_GetError()); exit(0); }

    if (TTF_Init() == -1){ printf("SDL_ttf init failure, SDL_ttf Error: %s\n", TTF_GetError()); }

	if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		printf( "Warning: Linear texture filtering not enabled!" );

    set_viewport(0, 0, display_size.w, display_size.h);

    printf("Renderer constructed\n");
}

Renderer::~Renderer()
{
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    window   = 0;
    renderer = 0;
    IMG_Quit();
    SDL_Quit();
    printf("Renderer destructed\n");
}

void Renderer::update(Game *game, Assets *assets)
{
    SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);
    SDL_RenderClear(renderer);

    SDL_RenderSetViewport(renderer, &viewport);
    SDL_SetRenderDrawColor(renderer, 200, 200, 200, 200);
    SDL_RenderFillRect(renderer, &viewport);

    if (!game->player.dead)
    {
        draw_sprite(game->player.bodysprite, game->player.x, game->player.y,
            game->player.bodysprite->clip, game->player.bodyangle,
            &game->player.body_origin, SDL_FLIP_NONE);
        draw_sprite(game->player.turretsprite,
            game->player.x, game->player.y,
            game->player.turretsprite->clip, game->player.turretangle,
                &game->player.turret_origin, SDL_FLIP_NONE);
    }

    if (!game->enemy.dead)
    {
        draw_sprite(game->enemy.bodysprite, game->enemy.x, game->enemy.y,
            game->enemy.bodysprite->clip, game->enemy.bodyangle,
            &game->enemy.body_origin, SDL_FLIP_NONE);
        draw_sprite(game->enemy.turretsprite,
            game->enemy.x, game->enemy.y,
            game->enemy.turretsprite->clip, game->enemy.turretangle,
                &game->enemy.turret_origin, SDL_FLIP_NONE);
    }
    for (int i = 0; i < game->bullets.size(); ++i)
    {
        if (game->bullets[i]->active)
        {
        draw_sprite(game->bullets[i]->sprite, game->bullets[i]->x,
            game->bullets[i]->y, game->bullets[i]->sprite->clip,
            game->bullets[i]->angle, 0, SDL_FLIP_NONE);
        }
    }

    SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
    SDL_RenderFillRect(renderer, &game->hpbg1_rec);
    SDL_RenderFillRect(renderer, &game->hpbg2_rec);

    SDL_SetRenderDrawColor(renderer, 0, 125, 0, 255);
    SDL_RenderFillRect(renderer, &game->hp1_rec);
    SDL_RenderFillRect(renderer, &game->hp2_rec);

    if (game->gamewinner == 1)
        draw_text(assets->texts[1], display_size.w / 2 - assets->texts[1]->clip->w / 2,
            display_size.h / 2 - 200, assets->c_green, assets->df_font);
    if (game->gamewinner == 2)
        draw_text(assets->texts[2], display_size.w / 2 - assets->texts[2]->clip->w / 2,
            display_size.h / 2 - 200, assets->c_red, assets->df_font);

    SDL_RenderPresent(renderer);
}

void Renderer::menu(Assets *assets)
{
    SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);
    SDL_RenderClear(renderer);

    SDL_RenderSetViewport(renderer, &viewport);
    SDL_SetRenderDrawColor(renderer, 200, 200, 200, 200);
    SDL_RenderFillRect(renderer, &viewport);

    draw_text(assets->texts[0], display_size.w / 2 - assets->texts[0]->clip->w / 2,
        display_size.h / 2 - 200, assets->c_black, assets->df_font);

    SDL_RenderPresent(renderer);
}

void Renderer::set_fullscreen(uint32_t flag)
{
    SDL_SetWindowFullscreen(window, flag);
}

SDL_Rect Renderer::get_display_size(int display)
{
    SDL_Rect rec;
    int w, h;
    float dpi, hpi, vpi;
    SDL_GetDisplayBounds(display, &rec);
    printf("screen_w: %d, screen_h: %d\n", rec.w, rec.h);

    return rec;
}

void Renderer::set_viewport(int x, int y, int w, int h)
{
    viewport.x = 0;
    viewport.y = 0;
    viewport.w = w;
    viewport.h = h;
}

void Renderer::draw_sprite(Sprite *sprite, int x, int y, SDL_Rect *clip,
    double angle, SDL_Point *center, SDL_RendererFlip flip)
{
    SDL_Rect renderquad;
    if (clip)
    {
        renderquad.x = x;
        renderquad.y = y;
        renderquad.w = clip->w;
        renderquad.h = clip->h;
    }
    SDL_RenderCopyEx(renderer, sprite->tex, clip, &renderquad, angle + 90, center, flip);
}

void Renderer::draw_text(Text *t, int x, int y, SDL_Color color, TTF_Font *font)
{
    SDL_DestroyTexture(t->tex);
    t->tex = 0;
    SDL_Surface *surface = TTF_RenderText_Solid(font, t->text.c_str(), color);
    if (!surface)
    {
        printf("surface load failed. SDL_image_error: %s\n", SDL_GetError());
        return;
    }
    t->tex = SDL_CreateTextureFromSurface(renderer, surface);
    if (!t->tex)
    {
        printf("texture creation failed. SDL Error: %s\n", SDL_GetError());
        return;
    }

    SDL_Rect clip;
    clip.x = 0;
    clip.y = 0;
    clip.w = surface->w;
    clip.h = surface->h;

    SDL_Rect renderquad;
    renderquad.x = x;
    renderquad.y = y;
    renderquad.w = clip.w;
    renderquad.h = clip.h;

    SDL_FreeSurface(surface);
    SDL_RenderCopy(renderer, t->tex, &clip, &renderquad);
}

int Assets::load_sprite(SDL_Renderer *renderer, string path)
{
    Sprite *spr = new Sprite;
    SDL_Rect *clip = new SDL_Rect;
    SDL_Surface *surface = IMG_Load(path.c_str());

    if (!surface)
        printf("Image load failed %s! SDL_image_error: %s\n",
            path.c_str(), SDL_GetError());
    else
    {
        SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, 0, 0xFF, 0xFF));
        spr->tex = SDL_CreateTextureFromSurface(renderer, surface);
        if (!spr->tex)
            printf("Sprite creation failed %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
        else
        {
            clip->x = 0;
            clip->y = 0;
            clip->w = surface->w;
            clip->h = surface->h;

            spr->clip = clip;
        }

        SDL_FreeSurface(surface);
        printf("Sprite %s load succesfull!\n", path.c_str());
        sprites.push_back(spr);
        printf("sprites size: %d\n", sprites.size());
    }

    return 0;
}

int Assets::create_text(SDL_Renderer *renderer, string text, TTF_Font *font, SDL_Color color)
{
    Text *t = new Text;
    SDL_Rect *clip = new SDL_Rect;
    SDL_Surface *surface = TTF_RenderText_Solid(font, text.c_str(), color);
    if (!surface)
    {
        printf("surface load failed. SDL_image_error: %s\n", SDL_GetError());
        return 2;
    }
    t->tex = SDL_CreateTextureFromSurface(renderer, surface);
    if (!t->tex)
    {
        printf("texture creation failed. SDL Error: %s\n", SDL_GetError());
        return 3;
    }
    clip->x = 0;
    clip->y = 0;
    clip->w = surface->w;
    clip->h = surface->h;
    t->clip = clip;
    t->text = text;
    texts.push_back(t);
    printf("texts size: %d\n", texts.size());
    SDL_FreeSurface(surface);
    printf("created text %s\n", text.c_str());
    return 0;
}

int Assets::load_font(string path, int size)
{
    df_font = TTF_OpenFont(path.c_str(), size);
    if (!df_font)
    {
        printf("failed to load font %s\n SDL_ttf Error: %s\n", path.c_str(), TTF_GetError());
        return 1;
    }
    else
    {
        printf("load font %s succesfull\n", path.c_str());
    }
    return 0;
}
