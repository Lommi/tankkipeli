#ifndef INPUT_H
#define INPUT_H
#include <SDL2/SDL.h>
#include <iostream>
#include <math.h>
#include "game.h"
#include "graphics.h"
#include "network.h"
#include "deps/types.h"
#include "deps/utils.h"

class Input;

class Input
{
    public:
    Input(){};
    ~Input(){};
    const Uint8 *keystate;
    bool space_pressed = 0;
    void update(Game *game, Renderer *renderer, Assets *assets, Network *network);
    void menu(Game *game);
};

#endif
