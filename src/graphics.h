#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <vector>
#include <string>
#include "deps/types.h"
#include "game.h"

using namespace std;

class  Game;
class  Renderer;
class  Assets;
struct Sprite;
struct Text;

class Renderer
{
    public:
    Renderer();
    ~Renderer();

    SDL_Window      *window;
    SDL_Renderer    *renderer;
    SDL_Rect        display_size;
    SDL_Rect        viewport;
    int             fps;

    void update(Game *game, Assets *assets);
    void menu(Assets *assets);
    void set_fullscreen(uint32_t flag);
    SDL_Rect get_display_size(int display);
    void set_viewport(int x, int y, int w, int h);
    void draw_sprite(Sprite *sprite, int x, int y, SDL_Rect *clip,
        double angle, SDL_Point *center, SDL_RendererFlip flip);
    void draw_text(Text *text, int x, int y, SDL_Color color, TTF_Font *font);
    int get_fps(){ return fps; };
};

struct Sprite
{
    SDL_Texture *tex;
    SDL_Rect    *clip;
    SDL_Point   *origin;
};

struct Text
{
    SDL_Texture *tex;
    SDL_Rect    *clip;
    string      text;
};

class Assets
{
    public:
    Assets(){};
    ~Assets(){};

    vector<Sprite*> sprites;
    vector<Text*>   texts;

    uint32  num_sprites = 0;
    uint32  num_texts = 0;

    TTF_Font  *df_font;
    Color c_white = {255, 255, 255};
    Color c_black = {0, 0, 0};
    Color c_red   = {125, 0, 0};
    Color c_green = {0, 125, 0};
    Color c_blue  = {0, 0, 125};

    int load_sprite(SDL_Renderer *renderer, string path);
    int create_text(SDL_Renderer *renderer, string text, TTF_Font *font, SDL_Color color);
    int load_font(string path, int size);
};

#endif
