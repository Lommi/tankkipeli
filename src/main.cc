#include <iostream>
#include <thread>
#include "globals.h"
#include "game.h"
#include "graphics.h"
#include "input.h"
#include "network.h"

int main(int argc, char **argv)
{
    Game game;
    Assets assets;
    Renderer renderer;
    Input input;
    Network network;

    network.init_sockets();
    thread net_thread(&Network::update, &network, &game);

    assets.load_sprite(renderer.renderer, "../assets/tank_main_noshadow.png");
    assets.load_sprite(renderer.renderer, "../assets/tank_tower_noshadow.png");
    assets.load_sprite(renderer.renderer, "../assets/bullet.png");
    assets.load_font("../assets/consola.ttf", 34);
    assets.create_text(renderer.renderer, "PRESS ENTER",
        assets.df_font, assets.c_black);
    assets.create_text(renderer.renderer, "VICTORY!",
        assets.df_font, assets.c_green);
    assets.create_text(renderer.renderer, "DEFEAT!",
        assets.df_font, assets.c_red);

    game.display_size_w = renderer.display_size.w; //lol

    game.init_stats();
    for (int i = 0; i < 20; ++i)
        game.create_bullet(assets.sprites[2], 0, 0, 8, 0);
    game.player = game.create_player(assets.sprites[0], assets.sprites[1],
        renderer.display_size.w / 2, renderer.display_size.h / 2);
    game.enemy = game.create_player(assets.sprites[0], assets.sprites[1],
        renderer.display_size.w / 2, renderer.display_size.h / 2);

    while (game.state == 0)
    {
        input.menu(&game);
        renderer.menu(&assets);
    }
    while (game.mainloop)
    {
        input.update(&game, &renderer, &assets, &network);
        game.update(&renderer, &network);
        renderer.update(&game, &assets);
    }

    network.shutdown_socket();

    while(network.network_active)
        this_thread::yield();
    net_thread.join();
    printf("net thread joined\n");

    return 0;
}
