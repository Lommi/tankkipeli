#ifndef GAME_H
#define GAME_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <vector>
#include "graphics.h"
#include "network.h"
#include "deps/utils.h"

class  Assets;
class  Network;
class  Renderer;
struct Sprite;
struct Bullet;
class  Game;
class  Player;

struct Bullet
{
    bool   active;
    Sprite *sprite;
    int    x, y, id;
    float  speed;
    double angle;
};

class Player
{
    public:
    Player(){ dead = 0; };
    ~Player(){};
    Sprite *bodysprite;
    Sprite *turretsprite;
    float x, y, turret_xoffset, turret_yoffset;
    SDL_Point turret_origin;
    SDL_Point body_origin;
    float drivespeed = 2;
    float turnspeed = 2;
    double bodyangle, turretangle;
    bool dead;
};

class Game
{
    public:
    Game(){};
    ~Game() { printf("Game shutdown\n"); };
    Player player;
    Player enemy;
    vector<Bullet*> bullets;
    int state = 0;
    int num_bullets;
    int bullet_idx;
    bool mainloop;
    int display_size_w;

    int gamewinner;
    int p1_hp, p2_hp;
    SDL_Rect hp1_rec, hp2_rec, hpbg1_rec, hpbg2_rec;

    bool init_stats();
    Player create_player(Sprite *bodysprite, Sprite *turretsprite, int x, int y);
    int create_bullet(Sprite *sprite, int x, int y, float speed, double angle);
    void update(Renderer *renderer, Network *network);
};

#endif
